#!/bin/bash
# Get an AFS token for the given user.
#
# This is used to deliver mail with the appropriate credentials.
#
# Usage:
#
# get-token $USER
# - If user is root, call this script as $USER
#
# get-token $USER norecurse
# - Don't recursively call this script, even if user if root

REALUSER=$(whoami)
USER=$1
LOGFILE=/tmp/exim4/weird-error.log

if test "$REALUSER" = "root"; then
    if test "$2" = "norecurse"; then
        echo "Error: running as root even after trying to change to $USER" \
            >> $LOGFILE
        exit 1
    fi

    # Decide whether the user exists: getent returns 0 error code if so
    getent passwd "$USER" >/dev/null
    if test $? -ne 0; then
        echo "$USER is not a local user, so ignoring them" \
            >> $LOGFILE
        exit 1
    else
        USER=$(getent passwd "$1" | cut -d':' -f 1)
        exec su $USER -c "$0 $1 norecurse"
    fi
fi

# Make sure USER exists, and resolve UIDs to a login name
USER=$(getent passwd "$USER" | cut -d':' -f 1)
LOGFILE=/tmp/exim4/get-token-log.$USER

if test -z "$USER"; then
    echo "$USER is not a local user, so ignoring them" \
        >> /tmp/exim4/weird-error.log
    exit 1
fi

# fuse stdin and stderr
exec 2>&1

# all future output goes to this file
exec >& $LOGFILE

# print name of user
echo "Running as user $REALUSER"

# debugging output
if test "$2" = "debug"; then
    shift; shift
    echo "Debugging output: $*"
fi

# set the credentials cache
export KRB5CCNAME=FILE:/tmp/exim4/krb5cc_$USER.email

# eliminate any previous tokens
kdestroy
unlog
KEYTAB=/etc/keytabs/user.daemon/$USER

# display command-to-be-invoked as a sanity check
echo kinit -kt $KEYTAB $USER/daemon@HCOOP.NET

kinit -kt $KEYTAB $USER/daemon@HCOOP.NET
aklog

# list tokens, for the sake of debugging
#tokens
